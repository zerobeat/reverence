﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reverence.Dto;
using Reverence.Logic;

namespace Reverence.Reverence
{
    public class SearchController : ServiceController
    {
        public ActionResult Search(string query)
        {
            Func<object> service;
            if (RequestMetaData.ApiVersion > "5.0.4")
            {
                service = Fail;
            }
            else if (RequestMetaData.ApiVersion > "3.0")
            {
                service = NewSearchMethod;
            }
            else
            {
                service = OldSearchMethod;
            }

            return Send(service);
        }

        public string OldSearchMethod()
        {
            return "Using old method";
        }

        public string NewSearchMethod()
        {
            return "Using new method";
        }

        public object Fail()
        {
            throw new FriendlyError() { ServiceError = "Fail" };
        }
    }
}
