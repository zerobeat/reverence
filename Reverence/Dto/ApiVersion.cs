﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProtoBuf;

namespace Reverence.Dto
{
    [ProtoContract]
    public class ApiVersion
    {
        // MOVE TO CONFIG
        public static readonly ApiVersion CurrentVersion = new ApiVersion("2.0.6");

        public ApiVersion(string input)
        {
            if (input.ICanHas())
            {
                var data = input.Split(new[] {'.'}, StringSplitOptions.RemoveEmptyEntries);

                Major = data.SafeGet(0, "0").ParseToInt();
                Minor = data.SafeGet(1, "0").ParseToInt();
                Fix = data.SafeGet(2, "0").ParseToInt();
            }
            else
            {
                Major = CurrentVersion.Major;
                Minor = CurrentVersion.Minor;
                Fix = CurrentVersion.Fix;
            }
        }
        [ProtoMember(1)]
        public int Major { get; set; }
        [ProtoMember(2)]
        public int Minor { get; set; }
        [ProtoMember(3)]
        public int Fix { get; set; }

        private static bool CompareVersion(int x, int y)
        {
            if (x < y)
                return true;
            if (x > y)
                return false;
            return false;
        }

        public static bool operator ==(ApiVersion x, ApiVersion y)
        {
            return x.Major == y.Major && x.Minor == y.Minor && x.Fix == y.Fix;
        }

        public static bool operator !=(ApiVersion x, ApiVersion y)
        {
            return !(x == y);
        }

		public override int GetHashCode ()
		{
			return base.GetHashCode ();
		}

		public override bool Equals (object obj)
		{
			return obj is ApiVersion && (ApiVersion)obj == this;
		}

        public static implicit operator ApiVersion(string version)
        {
            return new ApiVersion(version);
        }

        public static bool operator <(ApiVersion x, ApiVersion y)
        {
            if (CompareVersion(x.Major, y.Major))
                return true;
            if (CompareVersion(x.Minor, y.Minor))
                return true;
            if (CompareVersion(x.Fix, y.Fix))
                return true;
            return false;
        }

        public static bool operator >(ApiVersion x, ApiVersion y)
        {
            return !(x < y) && (x != y);
        }
    }
}