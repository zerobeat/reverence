﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reverence.Dto
{
    public class FriendlyError : Exception
    {
        public ServiceError ServiceError { get; set; }
        public Exception Exception { get; set; }
    }
}