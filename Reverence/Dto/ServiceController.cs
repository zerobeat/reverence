﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reverence.Logic;
using Reverence.Serializer;

namespace Reverence.Dto
{
    public abstract class ServiceController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Errors = new List<ServiceError>();
            if (ControllerContext.RouteData.Values["version"].ICanHas())
                RequestMetaData.ApiVersion = new ApiVersion((string)ControllerContext.RouteData.Values["version"]);
            _response.ResponseVersion = RequestMetaData.ApiVersion;
            base.OnActionExecuting(filterContext);
        }

        private HttpContextBase Context
        {
            get { return ControllerContext.HttpContext; }
        }

        public List<ISerializer> Serializers
        {
            // Configurable, with no initialization
            get { return new List<ISerializer>
                         {
                             new JsonSerializer(),
                             new ProtobufSerializer()
                         }; }
        }

        private ServiceResponse _response = new ServiceResponse();

        public List<ServiceError> Errors
        {
            get { return _response.Errors; }
            set { _response.Errors = value; }
        }

        public ActionResult Send(Func<object> method)
        {
            object result = null;
            try
            {
                result = method();
            }
            catch (Exception ex)
            {
                if (ex is FriendlyError)
                    Errors.Add((ex as FriendlyError).ServiceError);
                else
                    Errors.Add(new ServiceError
                    {
                        Title = "Unhandled Exception"
                    }); 
            }
            return Send(result);
        }

        public ActionResult Send(object value)
        {
            _response.Result = value;

            if (Errors.Count > 0)
                _response.StatusCode = ServiceResponse.StatusCodes.Error;
            else
                _response.StatusCode = ServiceResponse.StatusCodes.Success;

            var responseFormat = RequestMetaData.ApplicationTypes;

            var serializer = Serializers.FirstOrDefault(s => responseFormat.Contains(s.ApplicationType));

            if (serializer.ICanHas())
                return Content(serializer.Serialize(_response), serializer.ApplicationType);
            return Content("Could not server application-type: " + String.Join(" ", responseFormat), "text/html");
            
        }
    }
}