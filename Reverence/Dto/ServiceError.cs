﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProtoBuf;

namespace Reverence.Dto
{
    [ProtoContract]
    public class ServiceError
    {
        [ProtoMember(1)]
        public string Title { get; set; }

        public static implicit operator ServiceError(string friendlyMessage)
        {
            return new ServiceError
                   { Title = friendlyMessage };
        }
    }
}