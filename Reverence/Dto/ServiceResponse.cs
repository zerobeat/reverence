﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProtoBuf;

namespace Reverence.Dto
{
    [ProtoContract]
    public class ServiceResponse
    {
        public enum StatusCodes { Success, Error }

        [ProtoMember(1)]
        public List<ServiceError> Errors { get; set; }

        [ProtoMember(2, DynamicType = true)]
        public object Result { get; set; }

        [ProtoMember(3)]
        public StatusCodes StatusCode { get; set; }

        [ProtoMember(4)]
        public ApiVersion ResponseVersion { get; set; }
    }
}