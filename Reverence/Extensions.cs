﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reverence
{
    public static class Extensions
    {
        public static int ParseToInt(this string input, int defaultValue=0)
        {
            int result;
            if (Int32.TryParse(input, out result))
                return result;
            return defaultValue;
        }

        public static bool ICanHas(this object input)
        {
            return input != default(object);
        }

        public static T SafeGet<T>(this T[] input, int i, T defaultValue)
        {
            if (input.Length > i)
                return input[i];
            return defaultValue;
        }
    }
}