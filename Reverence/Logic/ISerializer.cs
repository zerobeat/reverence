﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Reverence.Dto;

namespace Reverence.Logic
{
    public interface ISerializer
    {
        string ApplicationType { get; }
        string Serialize(ServiceResponse response);
    }
}