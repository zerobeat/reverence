﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Reverence.Dto;

namespace Reverence.Logic
{
    public static class RequestMetaData
    {
        private const string ParameterPrefix = "meta-";

        public static string GetValue(string name)
        {
            var context = HttpContext.Current;

            if (context.Request.Headers[name].ICanHas())
                return context.Request.Headers[name];

            if (context.Request.QueryString[ParameterPrefix + name].ICanHas())
                return context.Request.QueryString[ParameterPrefix + name];

            if (context.Request.Form[ParameterPrefix + name].ICanHas())
                return context.Request.Form[ParameterPrefix + name];

            var httpCookie = context.Request.Cookies[ParameterPrefix + name];
            if (httpCookie.ICanHas())
                return httpCookie.Value;

            return null;
        }

        public static Guid Token
        {
            get
            {
                Guid token;
                Guid.TryParse(GetValue("Token"), out token);
                return token;
            }
        }

        public static string[] ApplicationTypes
        {
            get
            {
                var types = GetValue("type");
                if (types.ICanHas())
                    return new [] {types};
                return HttpContext.Current.Request.AcceptTypes;
            }
        }

        private static ApiVersion _version;
        public static ApiVersion ApiVersion
        {
            get { 
                if (!_version.ICanHas())
                    _version = new ApiVersion(GetValue("Api-Version"));
                return _version;
            }
            set { _version = value; }
		}
		
		public static int SiteId
		{
			get { return GetValue("SiteId").ParseToInt(1); }
		}
		
		public static string ClientKey
		{
			get { return GetValue("Client-Key"); }
		}
    }
}