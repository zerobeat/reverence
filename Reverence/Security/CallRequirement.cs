using System;
using System.Web.Mvc;
using Reverence.Logic;
using Reverence.Dto;

namespace Reverence
{
	public class CallRequirement : ActionFilterAttribute
	{
		public CallRequirement (CallRequirements requirement)
		{
			_requirements = new[] { requirement };
		}
		
		public CallRequirement (CallRequirements[] requirements)
		{
			_requirements = requirements;
		}
		
		public CallRequirements[] _requirements;

		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			var controller = (ServiceController)filterContext.Controller;

			var missingRequirements = Settings.UserTokenProvider.ValidateCallRequirement (_requirements);
			
			foreach (var requirement in missingRequirements) {
				controller.Errors.Add (String.Format("Call does not meet requirement {0}", 
				                                     requirement.ToString()));
			} 
		}
	}
}

