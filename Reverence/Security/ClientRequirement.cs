using System;
using System.Web.Mvc;
using Reverence.Dto;
using Reverence.Logic;

namespace Reverence
{
	public class ClientRequirement : ActionFilterAttribute
	{
		public ClientRequirement (ClientRequirements requirement)
		{
			_requirements = new[] { requirement };
		}

		public ClientRequirement (ClientRequirements[] requirements)
		{
			_requirements = requirements;
		}

		public ClientRequirements[] _requirements;

		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			var controller = (ServiceController)filterContext.Controller;

			if (Settings.ClientKeyProvider.ValidateClientKey (RequestMetaData.ClientKey)) {
				controller.Errors.Add (String.Format("Client key is invalid"));
				return;
			}

			var missingRequirements = Settings.ClientKeyProvider.ValidateClientRequirement (
				RequestMetaData.ClientKey, _requirements);

			foreach (var requirement in missingRequirements) {
				controller.Errors.Add (String.Format("Client do not have permission {0}", 
				                                     requirement.ToString()));
			} 
		}
	}
}

