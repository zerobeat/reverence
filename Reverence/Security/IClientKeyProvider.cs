using System;

namespace Reverence
{
	public interface IClientKeyProvider
	{
		bool ValidateClientKey(string key);
		ClientRequirements[] ValidateClientRequirement(string key, ClientRequirements[] requirements);
	}
}