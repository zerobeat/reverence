using System;

namespace Reverence
{
	public interface IUserTokenProvider
	{
		Guid CreateToken(int userId, Guid deviceId, TimeSpan lifetime);
		int GetUser(Guid token);
		void DeleteToken(Guid token);
		void RenewToken(Guid token, TimeSpan lifetime);
		ClientRequirements[] ValidateCallRequirement(CallRequirements[] requirements);
	}
}

