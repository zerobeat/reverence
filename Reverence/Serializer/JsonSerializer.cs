﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Reverence.Dto;
using Reverence.Logic;
using Newtonsoft.Json;

namespace Reverence.Serializer
{
    public class JsonSerializer : ISerializer
    {
        public string ApplicationType
        {
            get { return "application/json"; }
        }

        public string Serialize(ServiceResponse response)
        {
            return JsonConvert.SerializeObject(response);
        }
    }
}