﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Reverence.Dto;
using Reverence.Logic;

namespace Reverence.Serializer
{
    public class ProtobufSerializer : ISerializer
    {
        public string ApplicationType
        {
            get { return "application/x-protobuf"; }
        }

        public string Serialize(ServiceResponse respone)
        {
            using (var writer = new MemoryStream())
            {
                ProtoBuf.Serializer.Serialize(writer, respone);
                using (var reader = new StreamReader(writer))
                {
                    writer.Position = 0;
                    return reader.ReadToEnd();
                }
            }
        }
    }
}