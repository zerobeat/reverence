using System;

namespace Reverence
{
	public class Settings
	{
		public static IClientKeyProvider ClientKeyProvider { get; set; }
		public static IUserTokenProvider UserTokenProvider { get; set; }
	}
}

